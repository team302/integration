package com.agh.sius.model.info;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;


@Data
@XmlRootElement
public class WeatherInfo extends Info implements Serializable {
    public Double temperature;
}
