package com.agh.sius.model.info;

import lombok.Data;

import java.util.Date;

@Data
public abstract class Info {
    public Date date;
    public String street;
}
