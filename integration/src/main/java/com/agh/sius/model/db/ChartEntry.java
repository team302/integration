package com.agh.sius.model.db;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@IdClass(ChartEntryPK.class)
public class ChartEntry {
    @Id
    @Column
    private Date baseDate;

    @Id
    @Column
    private String street;

    @Column
    private Date date;

    @Column
    private Double temperature;

    @Column
    private Integer pM10;

    @Column
    private Integer caqI10;

    @Column
    private Double traffic;

}
