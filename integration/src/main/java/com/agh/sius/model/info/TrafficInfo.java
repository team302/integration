package com.agh.sius.model.info;

import lombok.Data;

public class TrafficInfo extends Info {
    public Double value;
}
