package com.agh.sius.model.front;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class DateEntry {
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm")
    public Date date;
    public Double temperature;
    public Integer pM10;
    public Integer caqI10;
    public Double traffic;
}
