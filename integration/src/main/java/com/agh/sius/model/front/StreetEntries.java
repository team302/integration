package com.agh.sius.model.front;

import java.util.ArrayList;
import java.util.List;

public class StreetEntries {
    public String street;
    public List<DateEntry> dateEntries = new ArrayList<>();

    public StreetEntries(String street) {
        this.street = street;
    }
}
