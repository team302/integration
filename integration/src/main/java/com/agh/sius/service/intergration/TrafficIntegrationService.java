package com.agh.sius.service.intergration;

import com.agh.sius.model.db.ChartEntry;
import com.agh.sius.model.info.TrafficInfo;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

@Service
public class TrafficIntegrationService extends IntegrationService<TrafficInfo> {

    @Override
    protected void updateFields(ChartEntry chartEntry, TrafficInfo info) {
        chartEntry.setTraffic(info.value);
    }
}
