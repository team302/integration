package com.agh.sius.service;

import com.agh.sius.model.info.PollutionInfo;
import com.agh.sius.model.info.TrafficInfo;
import com.agh.sius.model.info.WeatherInfo;
import com.agh.sius.service.intergration.PollutionIntegrationService;
import com.agh.sius.service.intergration.TrafficIntegrationService;
import com.agh.sius.service.intergration.WeatherIntegrationService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Receiver {
    @Autowired
    WeatherIntegrationService weatherIntegrationService;
    @Autowired
    PollutionIntegrationService pollutionIntegrationService;
    @Autowired
    TrafficIntegrationService trafficIntegrationService;

    @RabbitListener(queues = "weatherQueue")
    public void receiveWeatherInfo(WeatherInfo message) {
        weatherIntegrationService.store(message);
    }

    @RabbitListener(queues = "pollutionQueue")
    public void receivePollutionInfo(PollutionInfo message) {
        pollutionIntegrationService.store(message);
    }

    @RabbitListener(queues = "trafficQueue")
    public void receiveTrafficInfo(TrafficInfo message) {
        try {
            System.out.println("przyszet tfarric" + message.date);
            trafficIntegrationService.store(message);
        }catch (Throwable ignored) {
            ignored.printStackTrace();
            throw ignored;
        }
    }
}
