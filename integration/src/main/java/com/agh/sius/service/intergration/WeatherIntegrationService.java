package com.agh.sius.service.intergration;

import com.agh.sius.model.db.ChartEntry;
import com.agh.sius.model.info.WeatherInfo;
import org.springframework.stereotype.Service;

@Service
public class WeatherIntegrationService extends IntegrationService<WeatherInfo> {

    @Override
    protected void updateFields(ChartEntry chartEntry, WeatherInfo info) {
        chartEntry.setTemperature(info.temperature);
    }
}
