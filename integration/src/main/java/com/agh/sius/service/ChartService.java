package com.agh.sius.service;

import com.agh.sius.model.db.ChartEntry;
import com.agh.sius.service.persistence.ChartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChartService {
    @Autowired
    ChartRepository chartRepository;

    public List<ChartEntry> getForStreet(String street) {
        return chartRepository.findByStreet(street);
    }
}
