package com.agh.sius.service;

import com.agh.sius.model.info.PollutionInfo;
import com.agh.sius.model.info.TrafficInfo;
import com.agh.sius.model.info.WeatherInfo;
import org.joda.time.DateTime;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Component
public class MockRabbitSender {
    List<String> STREETS = Arrays.asList("os. Piastów", "ul. Bujaka", "ul. Bulwarowa", "ul. Dietla", "ul. Złoty Róg", "Al. Krasińskiego");

//    @Autowired
//    RabbitTemplate rabbitTemplate;

//    @Scheduled(fixedDelay = 60 * 100)
//    public void sendToWeather() {
//        WeatherInfo weatherInfo = new WeatherInfo();
//        weatherInfo.temperature = 12.5 + new Random().nextInt(10);
//        weatherInfo.date = DateTime.now().toDate();
//        weatherInfo.street = STREETS.get(new Random().nextInt(STREETS.size()));
//        rabbitTemplate.convertAndSend("weatherQueue", weatherInfo);
//    }
//
//    @Scheduled(fixedDelay = 60 * 100)
//    public void sendToPollution() {
//        PollutionInfo pollutionInfo = new PollutionInfo();
//        pollutionInfo.pM10 = 12 + new Random().nextInt(10);
//        pollutionInfo.caqI10 = 12 + new Random().nextInt(10);
//        pollutionInfo.date = DateTime.now().toDate();
//        pollutionInfo.street = STREETS.get(new Random().nextInt(STREETS.size()));
//        rabbitTemplate.convertAndSend("pollutionQueue", pollutionInfo);
//    }

//    @Scheduled(fixedDelay = 60 * 100)
//    public void sendToTraffic() {
//        TrafficInfo trafficInfo = new TrafficInfo();
//        trafficInfo.date = DateTime.now().toDate();
//        trafficInfo.street = STREETS.get(new Random().nextInt(STREETS.size()));
//        trafficInfo.value = new Random().nextDouble() * 10;
//        rabbitTemplate.convertAndSend("trafficQueue", trafficInfo);
//    }
}
