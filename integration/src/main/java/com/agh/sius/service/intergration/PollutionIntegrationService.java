package com.agh.sius.service.intergration;

import com.agh.sius.model.db.ChartEntry;
import com.agh.sius.model.info.PollutionInfo;
import org.springframework.stereotype.Service;

@Service
public class PollutionIntegrationService extends IntegrationService<PollutionInfo> {

    @Override
    protected void updateFields(ChartEntry chartEntry, PollutionInfo info) {
        chartEntry.setCaqI10(info.caqI10);
        chartEntry.setPM10(info.pM10);
    }
}
