package com.agh.sius.service.intergration;

import com.agh.sius.model.db.ChartEntry;
import com.agh.sius.model.info.Info;
import com.agh.sius.model.info.TrafficInfo;
import com.agh.sius.service.persistence.ChartRepository;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

public abstract class IntegrationService<T extends Info> {
    @Autowired
    ChartRepository chartRepository;

    @Transactional
    public void store(T info) {
        if (info instanceof TrafficInfo) {
            info.date = new DateTime(info.date).plusHours(2).toDate();
        }

        Date baseDate = getBaseDate(info.date);
        ChartEntry chartEntry = chartRepository.findByStreetAndBaseDate(info.street, baseDate);
        if (chartEntry == null) {
            chartEntry = new ChartEntry();
            chartEntry.setStreet(info.street);
            chartEntry.setBaseDate(baseDate);
        }
        chartEntry.setDate(info.date);
        updateFields(chartEntry, info);
        chartRepository.save(chartEntry);
    }

    // For now merge to minute
    private Date getBaseDate(Date date) {
        return new DateTime(date).withMinuteOfHour(0).withSecondOfMinute(0).withMillisOfSecond(0).toDate();
    }

    protected abstract void updateFields(ChartEntry chartEntry, T info);

}
