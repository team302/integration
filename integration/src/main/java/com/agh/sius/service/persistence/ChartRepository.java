package com.agh.sius.service.persistence;

import com.agh.sius.model.db.ChartEntry;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

@Repository
public interface ChartRepository extends CrudRepository<ChartEntry, Long> {
    ChartEntry findByStreetAndBaseDate(String street, Date baseDate);
    List<ChartEntry> findByDateBetween(Date date1, Date date2);
    List<ChartEntry> findByStreet(String street);
}
