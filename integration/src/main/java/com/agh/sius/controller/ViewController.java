package com.agh.sius.controller;

import com.agh.sius.service.ChartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/view")
public class ViewController {
    @Autowired
    ChartService chartService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getMainPage() {
        return "index";
    }

    @RequestMapping(value = "/weather", method = RequestMethod.GET)
    public String getWeatherPage() {
        return "weather";
    }

    @RequestMapping(value = "/pollution", method = RequestMethod.GET)
    public String getPollutionPage() {
        return "pollution";
    }

    @RequestMapping(value = "/traffic", method = RequestMethod.GET)
    public String getTrafficPage() {
        return "traffic";
    }

    @RequestMapping(value = "/mixed", method = RequestMethod.GET)
    public String getMixedPage() {
        return "mixed";
    }

    @RequestMapping(value = "/mixedtraffic", method = RequestMethod.GET)
    public String getMixedTrafficPage() {
        return "mixedtraffic";
    }
}
