package com.agh.sius.controller;

import com.agh.sius.model.db.ChartEntry;
import com.agh.sius.model.front.DateEntry;
import com.agh.sius.model.front.StreetEntries;
import com.agh.sius.service.persistence.ChartRepository;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/chart")
public class ChartController {
    @Autowired
    ChartRepository chartRepository;

    @RequestMapping(value = "/weather/today", method = RequestMethod.POST)
    public List<StreetEntries> getWeatherChartEntriesForToday() {
        Date startOfDay = DateTime.now().withTimeAtStartOfDay().minusDays(10).toDate();
        Date endOfDay = DateTime.now().plusDays(1).withTimeAtStartOfDay().toDate();


        List<ChartEntry> chartEntries = chartRepository.findByDateBetween(startOfDay, endOfDay);
        return groupByStreets(chartEntries);
    }

    @RequestMapping(value = "/pollution/today", method = RequestMethod.POST)
    public List<StreetEntries> getPollutionChartEntriesForToday() {
        Date startOfDay = DateTime.now().withTimeAtStartOfDay().minusDays(10).toDate();
        Date endOfDay = DateTime.now().plusDays(1).withTimeAtStartOfDay().toDate();


        List<ChartEntry> chartEntries = chartRepository.findByDateBetween(startOfDay, endOfDay);
        return groupByStreets(chartEntries);
    }

    @RequestMapping(value = "/traffic/today", method = RequestMethod.POST)
    public List<StreetEntries> getTrafficChartEntriesForToday() {
        Date startOfDay = DateTime.now().withTimeAtStartOfDay().minusDays(10).toDate();
        Date endOfDay = DateTime.now().plusDays(1).withTimeAtStartOfDay().toDate();


        List<ChartEntry> chartEntries = chartRepository.findByDateBetween(startOfDay, endOfDay);
        return groupByStreets(chartEntries);
    }

    private List<StreetEntries> groupByStreets(List<ChartEntry> chartEntries) {
        Map<String, StreetEntries> streetEntries = new HashMap<>();
        Collections.sort(chartEntries, (o1, o2) -> o1.getBaseDate().compareTo(o2.getBaseDate()));
        for (ChartEntry chartEntry : chartEntries) {
            String street = chartEntry.getStreet();
            if (!streetEntries.containsKey(street)) {
                streetEntries.put(street, new StreetEntries(street));
            }
            DateEntry dateEntry = new DateEntry();
            dateEntry.setDate(new DateTime(chartEntry.getBaseDate()).plusHours(2).toDate());
            dateEntry.setTemperature(chartEntry.getTemperature());
            dateEntry.setPM10(chartEntry.getPM10());
            dateEntry.setCaqI10(chartEntry.getCaqI10());
            dateEntry.setTraffic(chartEntry.getTraffic());
            streetEntries.get(street).dateEntries.add(dateEntry);
        }
        return new ArrayList<>(streetEntries.values());
    }


}
