package com.agh.sius.configuration;

import com.agh.sius.configuration.queue.Pollution;
import com.agh.sius.configuration.queue.Traffic;
import com.agh.sius.configuration.queue.Weather;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableAsync
@EnableScheduling
public class MockRabbitSenderConfig {

    // Weather

    @Bean
    @Weather
    Queue weatherQueue() {
        return new Queue("weatherQueue", false);
    }

    @Bean
    @Weather
    TopicExchange weatherTopicExchange() {
        return new TopicExchange("weather-exchange");
    }

    @Bean
    @Weather
    Binding weatherQueueBinding(@Weather TopicExchange topicExchange, @Weather Queue queue) {
        return  BindingBuilder.bind(queue).to(topicExchange).with("weatherQueue");
    }

    // Pollution

    @Bean
    @Pollution
    Queue pollutionQueue() {
        return new Queue("pollutionQueue", false);
    }

    @Bean
    @Pollution
    TopicExchange pollutionTopicExchange() {
        return new TopicExchange("pollution-exchange");
    }

    @Bean
    @Pollution
    Binding pollutionQueueBinding(@Pollution TopicExchange topicExchange, @Pollution Queue queue) {
        return  BindingBuilder.bind(queue).to(topicExchange).with("pollutionQueue");
    }

    // Traffic

    @Bean
    @Traffic
    Queue trafficQueue() {
        return new Queue("trafficQueue", false);
    }

    @Bean
    @Traffic
    TopicExchange trafficTopicExchange() {
        return new TopicExchange("traffic-exchange");
    }

    @Bean
    @Traffic
    Binding trafficQueueBinding(@Traffic TopicExchange topicExchange, @Traffic Queue queue) {
        return  BindingBuilder.bind(queue).to(topicExchange).with("trafficQueue");
    }

}
