import requests
import pika
import pprint 
import json
import time
import datetime

JSON_FORMAT = '{"date": "{}","street": "{}","value": "{}"}'

BOX_DELTA = 0.05

class Street(object):

	def __init__(self, name, internalName, lat, long):
		self.name = name
		self.internalName = internalName
		self.lat = lat
		self.long = long

STREETS = [
		Street('Aleja Zygmunta Krasińskiego', 'Al. Krasińskiego', 50.0572, 19.9263),
		Street('Bulwarowa', 'ul. Bulwarowa', 50.078329, 20.050917),
		Street('Wincentego Witosa', 'ul. Bujaka', 50.009857, 19.951553),
		Street('Józefa Dietla', 'ul. Dietla', 50.054251, 19.943285),
		Street('Popielidów', 'os. Piastów', 50.078329, 20.050917),
		Street('Bronowicka', 'ul. Złoty Róg', 50.081712, 19.895475),		
]		

def formatJson(date, street, value):
	return '{"date": "' + str(date) + '","street": "' + str(street) + '","value": "' + str(value) +'"}'

class HereInterface(object):

	APP_ID = 'pPuVUY73L7Ek0fTyyWug'
	APP_CODE = '0a1BFSIYPYoMx42084E9Lg'

	FLOW_REQUEST_URL = 'https://traffic.cit.api.here.com/traffic/6.1/flow.json?bbox=51.5082,-0.1285;51.5062,-0.1265&app_id={}&app_code={}'

	def getFlowInfo(self, lat, long):
		pass



running = True
while running:
	try:
		connection = pika.BlockingConnection(pika.ConnectionParameters(
	               '127.0.0.1', 5672))
		channel = connection.channel()
		channel.queue_declare(queue='trafficQueue')
		trafficData = dict()
		for street in STREETS:
			response = requests.get('https://traffic.cit.api.here.com/traffic/6.1/flow.json?bbox={},{};{},{}&app_id=pPuVUY73L7Ek0fTyyWug&app_code=0a1BFSIYPYoMx42084E9Lg'.format( street.lat - BOX_DELTA, street.long - BOX_DELTA, street.lat + BOX_DELTA, street.long + BOX_DELTA))
			response.encoding = 'utf-8'
			data = json.loads( response.text)
			for rws in data['RWS']:
				for rw in rws['RW']:
					for fis in rw['FIS']:
						for fi in fis['FI']:
							if fi['TMC']['DE'] == street.name:
								if street.internalName not in data:
									trafficData[ street.internalName] = [0.0, 0]
								trafficData[ street.internalName][0] += float(fi['CF'][0]['JF'])
								trafficData[ street.internalName][1] += 1

		for streetInternalName, (sum, counter) in trafficData.items():
			channel.basic_publish(exchange='traffic-exchange', routing_key='trafficQueue', body=formatJson( datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M'), streetInternalName, str(sum/counter)))
		print ('Waiting for next task...')	
		connection.close()
		time.sleep( 1200)
	except Exception as e:
		print (e)
		time.sleep(10)

