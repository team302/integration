﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.Win32;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RabbitMQ.Client;
using WUnderground.Client;
using Timer = System.Timers.Timer;

namespace WeatherQueue
{
    class Program
    {
        private static JsonSerializer serializer = JsonSerializer.Create(new JsonSerializerSettings() {ContractResolver = new CamelCasePropertyNamesContractResolver(),DateFormatString = "yyyy-MM-dd HH:mm" });

        private static string[][] streets = {new []{"Al. Krasińskiego", "IKRAKOW5"},
            new [] { "ul. Bulwarowa", "IKRAKW10" },
            new [] {"ul. Bujaka","IKRAKW51"},
            new [] {"ul. Dietla","IKRAKW104"},
            new [] {"os. Piastów","IMALOPOL4"},
            new [] {"ul. Złoty Róg","IKRAKOW6"}};

        static void Main(string[] args)
        {
            WUndergroundClient.Config.ApiKey = "f1f3785749b2ac83";
            while (true)
            {
                Enqueue(null, null);
                Thread.Sleep(60 * 1000 * 30);
            }
        }

        private static void Enqueue(object sender, ElapsedEventArgs e)
        {
            var handle = GetOutputs();
            handle.Wait();
            using (var writer = new StringWriter())
            {
                foreach (var output in handle.Result)
                {
                    serializer.Serialize(writer, output);
                    Send(writer.ToString());
                    writer.GetStringBuilder().Clear();
                }
            }
            Console.WriteLine("Update sent on {0}", DateTime.Now);

        }


        private static async Task<List<Output>> GetOutputs()
        {
            var outputs = new List<Output>();
            foreach (var street in streets)
            {
                var result = await WUndergroundClient.GetConditionsForPersonalWeatherStationAsync(street[1]);
                var output = new Output();
                output.Temperature = result.current_observation.temp_c;
                output.Street = street[0];
                output.Date = DateTime.Parse(result.current_observation.observation_time_rfc822);
                outputs.Add(output);
            }
            return outputs;
        }

        private static void Send(string message)
        {
            var factory = new ConnectionFactory() { HostName = "192.168.202.103" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "weatherQueue",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);
                channel.QueueBind("weatherQueue","weather-exchange","");
                Console.WriteLine(message);
                var body = Encoding.UTF8.GetBytes(message);
                channel.BasicPublish(exchange: "weather-exchange",
                                     routingKey: "",
                                     basicProperties: null,
                                     body: body);
            }
        }
    }
    
}
