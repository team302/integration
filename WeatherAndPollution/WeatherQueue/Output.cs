﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherQueue
{
    class Output
    {
        public double Temperature { get; set; }
        public String Street { get; set; }
        public DateTime Date { get; set; }
    }
}
