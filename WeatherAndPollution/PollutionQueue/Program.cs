﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PollutionApi.Models;
using PollutionQueue.Model;
using RabbitMQ.Client;
using RabbitMQ.Client.Impl;
using Timer = System.Timers.Timer;

namespace PollutionQueue
{
    class Program
    {

        private static JsonSerializer serializer = JsonSerializer.Create(new JsonSerializerSettings() { ContractResolver = new CamelCasePropertyNamesContractResolver(), DateFormatString = "yyyy-MM-dd HH:mm" });

        static void Main(string[] args)
        {

            while (true)
            {
                Enqueue(null, null);
                Thread.Sleep(60*1000*30);
            }
        }

        private static void Enqueue(object sender, ElapsedEventArgs e)
        {
            var handler = GetRootObject();
            handler.Wait();
            var outputs = DaneToOutput(handler.Result.dane);
            using (var writer = new StringWriter())
            {
                foreach (var output in outputs)
                {
                    serializer.Serialize(writer, output);
                    Send(writer.ToString());
                    writer.GetStringBuilder().Clear();
                }
            }
            Console.WriteLine("Update sent on {0}", DateTime.Now);
        }

        private static async Task<RootObject> GetRootObject()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://powietrze.malopolska.pl/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var handler = await client.GetAsync("_powietrzeapi/api/dane?act=danemiasta&ci_id=01");
                if (handler.IsSuccessStatusCode)
                {
                   return await handler.Content.ReadAsAsync<RootObject>();

                }
            }
            return null;
        }

        private static List<Output> DaneToOutput(Dane dane)
        {
            var outputs = new List<Output>();
            foreach (var actual in dane.actual)
            {
                var output = new Output();
                output.Street = actual.station_name;
                output.City = dane.city.ci_name;
                output.Date = actual.details.First().o_czas;
                var pm10 = actual.details.FirstOrDefault(d => d.o_wskaznik == "pm10");
                if (pm10 != null)
                {
                    output.PM10 = pm10.o_value;
                    output.CAQI10 = pm10.caqi_value;
                }
                outputs.Add(output);
            }
            return outputs;
        }

        private static void Send(string message)
        {
            var factory = new ConnectionFactory() { HostName = "192.168.202.103" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "pollutionQueue",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);
                channel.QueueBind("pollutionQueue", "pollution-exchange", "");
                Console.WriteLine(message);
                var body = Encoding.UTF8.GetBytes(message);
                channel.BasicPublish(exchange: "pollution-exchange",
                                     routingKey: "",
                                     basicProperties: null,
                                     body: body);
            }
        }
    }
}
