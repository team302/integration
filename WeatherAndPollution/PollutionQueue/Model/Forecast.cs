﻿namespace PollutionApi.Models
{
    public class Forecast
    {
        public Wczoraj wczoraj { get; set; }
        public Dzisiaj dzisiaj { get; set; }
        public Jutro jutro { get; set; }
        public Pojutrze pojutrze { get; set; }
    }
}