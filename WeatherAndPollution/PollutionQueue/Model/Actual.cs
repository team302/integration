﻿using System.Collections.Generic;

namespace PollutionApi.Models
{
    public class Actual
    {
        public string station_id { get; set; }
        public string station_name { get; set; }
        public int station_hour { get; set; }
        public string station_max { get; set; }
        public List<Detail> details { get; set; }
    }
}