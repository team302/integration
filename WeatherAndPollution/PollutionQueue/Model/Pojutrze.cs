﻿using System.Collections.Generic;

namespace PollutionApi.Models
{
    public class Pojutrze
    {
        public List<Average> averages { get; set; }
        public string max { get; set; }
        public List<Detail> details { get; set; }
    }
}