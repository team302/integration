using System.Collections.Generic;

namespace PollutionApi.Models
{
    public class Dane
    {
        public City city { get; set; }
        public List<Actual> actual { get; set; }
        public Forecast forecast { get; set; }
        public Message message { get; set; }
    }
}