﻿using System;

namespace PollutionApi.Models
{
    public class Detail
    {
        public string o_id { get; set; }
        public string o_station { get; set; }
        public string st_id { get; set; }
        public string o_interval { get; set; }
        public string o_wskaznik { get; set; }
        public string par_id { get; set; }
        public int o_value { get; set; }
        public DateTime o_czas { get; set; }
        public string o_time { get; set; }
        public string g_id { get; set; }
        public string g_index { get; set; }
        public int caqi_value { get; set; }
        public string caqi_id { get; set; }
        public string aqi_value { get; set; }
        public string aqi_id { get; set; }
        public string g_min_val { get; set; }
        public string g_max_val { get; set; }
        public string g_nazwa { get; set; }
        public string par_name { get; set; }
        public string par_desc { get; set; }
        public string par_unit { get; set; }
        public string par_html { get; set; }
        public string parorder { get; set; }
        public string max { get; set; }
    }
}