namespace PollutionApi.Models
{
    public class Average
    {
        public string cf_id { get; set; }
        public string ci_id { get; set; }
        public string cf_type { get; set; }
        public string cf_date { get; set; }
        public string cf_max { get; set; }
        public string g_index { get; set; }
        public string caqi_value { get; set; }
        public string caqi_id { get; set; }
        public string aqi_value { get; set; }
        public string aqi_id { get; set; }
        public string cf_details { get; set; }
        public string max { get; set; }
    }
}