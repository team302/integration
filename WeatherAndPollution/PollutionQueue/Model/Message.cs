﻿namespace PollutionApi.Models
{
    public class Message
    {
        public int type { get; set; }
        public string message { get; set; }
    }
}