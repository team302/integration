﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PollutionQueue.Model
{
    class Output
    {
        public String City { get; set; }
        public String Street { get; set; }
        public DateTime Date { get; set; }
        public int PM10 { get; set; }
        public int CAQI10 { get; set; }
    }
}
