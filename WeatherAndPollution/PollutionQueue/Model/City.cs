namespace PollutionApi.Models
{
    public class City
    {
        public string ci_id { get; set; }
        public string co_id { get; set; }
        public string reg_id { get; set; }
        public string ci_name { get; set; }
        public string ci_county { get; set; }
        public string ci_countydesc { get; set; }
        public string ci_city { get; set; }
        public string ci_citydesc { get; set; }
        public string ci_location { get; set; }
        public string ci_locationdesc { get; set; }
        public string ci_lat { get; set; }
        public string ci_long { get; set; }
        public string ci_locationtype { get; set; }
        public string ci_locationparameters { get; set; }
        public string g_index { get; set; }
        public string aqi_id { get; set; }
        public string caqi_id { get; set; }
    }
}